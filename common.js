const API = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('.root');

const makeRequest = (url) => {
    return fetch(url);
}

function getFilm() {
    makeRequest(API)
        .then((response) => response.json())
        .then((films) => {
            films.forEach(({name, episodeId, characters, openingCrawl}) => {
                root.insertAdjacentHTML('afterbegin',
                    `<h2>${episodeId}. ${name}</h2>
                          <h3>${openingCrawl}</h3>
                          <h4 class="starring">Starring:</h4>
                          <div class="circle"></div>`);
                const starring = document.querySelector('.starring');
                const circle = document.querySelector('.circle');
                characters.forEach((item) => {
                    const character = (url) => {
                        return fetch(url);
                    }
                    character(item)
                        .then((response) => response.json())
                        .then(({name}) => {
                            setTimeout(()=>{
                                circle.remove();
                                const star = document.createElement('span');
                                starring.append(star);
                                star.textContent = `   ${name}, `;
                            },2000)
                        })
                })
            })
        })
}
getFilm();
